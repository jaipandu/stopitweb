from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from datetime import date, datetime, timedelta
import random, httplib, binascii, urllib, requests
import hashlib, json
from base64 import b64decode, b64encode
from onboarding.stopit_chat_client import chat_client
from django.views.decorators.csrf import csrf_exempt
from stopit_logging import logging

# Messenger constants
MESSENGER_URL = settings.MESSENGER_HTTP_PROTOCOL + settings.MESSENGER_HOST + ':' + str(settings.MESSENGER_HTTP_PORT) + '/http-bind/'
MESSENGER_HOST = settings.MESSENGER_HOST
API_ENDPOINT = settings.API_ENDPOINT_URL
PAGE_SIZE = 50


def get_messenger_session(request, chat_username, chat_password):
    logging.debug(' --------------- in get_messenger_session')

    if chat_username and chat_password:
        jid = chat_username + "@" + MESSENGER_HOST
        jid_password = chat_password
        messenger_client = chat_client(chat_username, jid_password)
        #messenger_client.bosh_connect(request)

        response_data = {}
        #response_data['rid'] = messenger_client.rid
        #response_data['sid'] = messenger_client.sid
        #response_data['jid'] = messenger_client.jabberid

        # save values in session for use by other pages
        #request.session['messenger_rid'] = messenger_client.rid
        #request.session['messenger_sid'] = messenger_client.sid
        request.session['messenger_host'] = MESSENGER_HOST
        request.session['messenger_url'] = MESSENGER_URL
        request.session['messenger_jid'] = chat_username
    
    return



def get_stanza_id(stanza):
   return stanza[stanza.find("id=")+4:stanza.find("\" ")] 


def get_chat_history(request, chat_username):
   chatHistoryURL = API_ENDPOINT+"/v1/intellicodes/getChatHistory"
   intellicode = request.session.get('user')
   response_to_frontend = {}

   if intellicode and chat_username:
       data = { "intellicode": str(intellicode),
                "chat_username": str(chat_username),
                "pageSize": str(PAGE_SIZE)
              }
       headers = {"content-type": "application/json",
                  "Authorization": "Basic " + b64encode(settings.API_AUTH_NAME+":"+settings.API_AUTH_PASS)}
       try:
           json_response = requests.post(chatHistoryURL, data=json.dumps(data), headers=headers)
           json_object = json_response.json()
           #test if response is bad
           if json_object['code'] != "100000":
               return {'code': json_object['code'], \
                               'message': json_object['message']}
           response_to_frontend['code'] = json_object['code']
           historyList = []
           for message in json_object['data']['chatHistory']:
               historyList.append({'id': get_stanza_id(message['stanza']), 'sentDate': str(message['sentDate']), 'body': message['body'], 'fromJID': message['fromJID']})
               response_to_frontend['chat_history'] = historyList
       except Exception, e:
            response_to_frontend = {'code': 200000, 'message': e[0]}

       return response_to_frontend

