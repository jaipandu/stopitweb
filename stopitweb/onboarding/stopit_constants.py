DOMAINS = {
    'com': 'com',
    'au': 'com.au',
    'nz': 'nz',
    'jp': 'jp',
}
COUNTRIES = {
    'com': 'US',
    'au': 'AU',
    'nz': 'NZ',
    'jp': 'JP',
}
