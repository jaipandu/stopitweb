from django.views.generic import View
from django.conf import settings
from django.shortcuts import render
from base64 import b64decode, b64encode
import requests, json
from stopit_logging import logging
import HTMLParser
from django.utils.translation import ugettext as _

def get_countries(request, error_message=None):
        params = {}
        logging.debug('Get countries got called')
        if error_message is not None:
            params["error_message"] = error_message
            logging.debug(error_message)
        try:
            try:
                org_type = request.GET['org_type']
            except:
                org_type = request.POST.get('org_type', 1)

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_response = requests.get(settings.API_ENDPOINT_URL + '/v1/countries', headers=json_headers)

            json_object = json_response.json()
            params["name"] = "success"
            params["country_name"] = json_object['data']['countries']
            params["org_type"] = org_type

            params["country_state_list"] = json.dumps(json_object['data'])

        except Exception as e:
            print str(e)
            params["name"] = "Django"
        return render(request, 'findcountry.html', params)


def get_states(request):
        params = {}
        logging.debug('Get states got called')
        try:
            try:
                org_type = request.GET['org_type']
                param_country = request.GET['code']
            except:
                org_type = request.POST['org_type']
                param_country = request.POST['code']

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_response = requests.get(settings.API_ENDPOINT_URL+ '/v1/countries', headers=json_headers)
            json_object = json_response.json()
            params["name"] = "success"
            all_countries = json_object['data']['countries']
            params["org_type"] = org_type
            for country in all_countries:
                if country['code'] == param_country:
                    params["states"] = country["states"]
                    params["country_code"] = param_country
                    return render(request, 'findstate.html', params)
            return get_states(request)
        except:
            params["name"] = "Django"
        return get_countries(request)


def get_orgs(request):
        params = {}
        logging.debug('Get orgs got called')
        try:
            try:
                org_type = request.GET['org_type']
                country_code = request.GET['country_code']
                state_code = request.GET['state_code']
            except:
                org_type = request.POST['org_type']
                country_code = request.POST['country_code']
                state_code = request.POST['state_code']

            json_data = {'country_id': country_code,
                         'state_id': state_code,
                         'org_name': "",
                         'org_type_id': org_type}

            if org_type == "1":
                params['IMAGE_FILE'] = 'school.png'
                params['ORG_TYPE_DESC'] = _('School')
            elif org_type == "2":
                params['IMAGE_FILE'] = 'college.png'
                params['ORG_TYPE_DESC'] = _('College')
            else:
                params['IMAGE_FILE'] = 'work.png'
                params['ORG_TYPE_DESC'] = _('Organization')

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            json_response = requests.post(settings.API_ENDPOINT_URL+ '/v1/orgs/search',
                                          data=json.dumps(json_data),
                                          headers=json_headers)

            json_object = json_response.json()
            params["orgs"] = json_object['data']['orgs']
            params["org_name"] = json_object['data']['orgs'][0]['org_name']
            params["org_type"] = org_type
            params["country_code"] = country_code
            params["state_code"] = state_code
            return render(request, 'findorg.html', params)
        except Exception as e:
            logging.debug(str(e))
            params["org_type"] = org_type
            params["code"] = country_code

            error_message = _("No organizations were found.")

            if str(org_type) == str(1):
                error_message = _("No schools were found.")
            elif str(org_type) == str(2):
                error_message = _("No colleges were found.")

            params["error_message"] = error_message;
            logging.debug(str(e))
        return get_countries(request, error_message)


def get_org_logininfo(request, org_name=None, error_message=None):
        params = {}
        logging.debug('Get orgs got called')
        logging.debug(org_name)

        try:
            if org_name is not None and error_message is None:
                html_parser = HTMLParser.HTMLParser()
                try:
                    org_type = request.GET['org_type']
                    country_code = request.GET['country_code']
                    state_code = request.GET['state_code']
                    org_id = request.GET['code']
                    roster_type = request.GET['roster_type']
                    roster_label = request.GET['roster_label']
                except Exception as e:
                    logging.debug(str(e))
                    org_type = request.POST['org_type']
                    logging.debug(org_type)
                    country_code = request.POST['country_code']
                    logging.debug(country_code)
                    state_code = request.POST['state_code']
                    logging.debug(state_code)
                    org_id = request.POST['code']
                    logging.debug(org_id)
                    roster_type = request.POST['roster_type']
                    logging.debug(roster_type)
                    roster_label = request.POST['roster_label']
                    logging.debug(roster_label)
            else:
                if error_message is not None:
                    logging.debug(error_message)
                    params["error_message"] = error_message
                return get_countries(request, error_message)

            params["org_type"] = org_type
            params["roster_type"] = roster_type
            params["roster_label"] = roster_label
            params["country_code"] = country_code
            params["state_code"] = state_code
            params["org_id"] = org_id
            params["org_name"] = org_name

            return render(request, 'enterlogininfo.html', params)
        except Exception as e:
            print str(e)
            logging.debug(str(e))
        return get_countries(request)
