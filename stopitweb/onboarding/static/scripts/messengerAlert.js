var getOfflineMessagesOtherInt = undefined;
var popupOpen = false;
function getOfflineChatsAjaxOtherPages() {
$.ajax( "/getOfflineMessages/" )
  .done(function(offlinePacket) {
      if(parseInt(offlinePacket.offMessagesCount, 10) > 0 && !popupOpen) {
          popupOpen = true;
          $("#dialog").html("You have one or more new messages pending. Click OK to see them.");
          $("#dialog").dialog({ 
    modal: true,
    buttons: [
    {
      text: "OK",
      click: function() {
        $("#dialog").html('');
        window.location.href = '/login/';
        $( this ).dialog( "close" );
      }
    }
  ]});
            $(".ui-dialog-title").html("STOPit Messenger");

      }
  });
}

var SESSION_LOGOUT_TIME =    300000;
var SESSION_ALERT_TIMEOUT = 1500000;	
var KEEP_LOGGED_IN = 0;
var sessionLogoutTimeout = undefined;
var sessionAlertTimeout = undefined;
var startSessionCountDown = function(keep_logged_in) {
      KEEP_LOGGED_IN = keep_logged_in;
      sessionAlertTimeout  = setTimeout(function () {
          // kick off the 5 minute countdown to force logout
          sessionLogoutTimeout = setTimeout(function () {
              if(!KEEP_LOGGED_IN) {
                  expireSession(1);
                  window.location.assign('../');
              }
              else {
                  expireSession(0);
                  location.reload();
              }
          }, SESSION_LOGOUT_TIME); 

          if($("#dialog").html() === "") {
              console.log("got here");
              // fire the alert to tell them they are about to be logged out
              $("#dialog").html(gettext('You will be logged out in 5 minutes unless you click "Stay Logged In"'));
              $("#dialog").dialog({ buttons: [
                {
                  text: gettext("Stay Logged In"),
                  click: function() {
                    recordActivity(KEEP_LOGGED_IN);
                    $("#dialog").html('');
                    $( this ).dialog( "close" );
                  }
 
                }
                                             ]});
            $(".ui-dialog-title").html(gettext("Please Note"));
          }
      }, SESSION_ALERT_TIMEOUT);

}

function expireSession(expire) {
    $.ajax({ url: "/expire/",
             dataType: "json",
             data: { expire : expire }
    }).success(function() {
            //success callback
            })
    .fail(function(exception) {
            console.log( exception );
            });

}

function recordActivity(keep_logged_in) {
    expireSession(0);
    clearTimeout(sessionLogoutTimeout);
    clearTimeout(sessionAlertTimeout);
    startSessionCountDown(keep_logged_in);
}

