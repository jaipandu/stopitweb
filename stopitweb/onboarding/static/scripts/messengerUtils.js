

var connection = null;
var startTime = null;
var messageIDs = {};
var checkMessagesInt = undefined;
var getOfflineMessagesInt = undefined;
window.stashedMsgs = [];
window.stashedMsgNum = 0;

function globalMsgIDsAdd(id){
    if(messageIDs.hasOwnProperty(id)){
        return true;
    }
    else {
        messageIDs[id] = 1;
        return false;
    }
}
function text_to_xml(text) {
    var doc = null;
    if (window['DOMParser']) {
        var parser = new DOMParser();
        doc = parser.parseFromString(text, 'text/xml');
    } else if (window['ActiveXObject']) {
        doc.async = false;
        doc.loadXML(text);
    } else {
        console.log("peekerror");
        throw {
type: 'PeekError',
          message: 'No DOMParser object found.'
        };
    }

    var elem = doc.documentElement;
    if ($(elem).filter('parsererror').length > 0) {
        return null;
    }
    return elem;
}

function stashMessage(from, msg, dateStr) {
    window.stashedMsgs[window.stashedMsgNum] = { 'from': from, 'msg': msg, 'dateStr': dateStr};
    window.stashedMsgNum++;
}

function log(from, msg, dateStr, fadeIn) {
    if(msg === undefined) {
        console.log("log called with undefined");
    }
    var directionClass = "Right"; 
    if(from) {
        directionClass = "Left";
    }
    var hideDisplayStr = "";
    if(fadeIn) {
        hideDisplayStr = 'id="latestMsg" style="display: none;"';
    }
                $('#log').append('<div class="bubble' + directionClass + '"  ' + hideDisplayStr + ' >' + '<span class="bubbleText">' + msg + '</span>' +'</div>');
                $('#log').append('<div class="chatDate' + directionClass + '" ' + hideDisplayStr + ' >' + dateStr.toLocaleString() + '</div>');
                $('div#latestMsg').fadeIn(1000);
                $('div#latestMsg').removeAttr('id');
}

function onConnect(status) 
{
    connection.addHandler(onMessage, null, 'message', null, null,  null); 
}

function onDisconnect(status) 
{
    connection.flush();
    connection.options.sync = true;
    connection.disconnect();
}

function onResult(iq) {
    var elapsed = (new Date()) - startTime;
    console.log(iq);
}

function onMessage(msg) {
    var id = msg.getAttribute('id');
    alreadyReceived = globalMsgIDsAdd(id);
    if(alreadyReceived){
        return true;
    }
    var to = msg.getAttribute('to');
    var from = msg.getAttribute('from');
    var type = msg.getAttribute('type');
    var sentDate = Date.now();
    var elems = msg.getElementsByTagName('body');

    //play audio beep
    var audioTag = document.getElementById("chatAudio");
    audioTag.play();

    if (type == "chat" && elems.length > 0) {
                var body = elems[0];
                if(!document.hasFocus() || $("#messengerWindow").css("display") == "none") {
                // stash messages for later display
                stashMessage(from, Strophe.getText(body), convertUTCDateToLocalDate(new Date(sentDate)).toLocaleString());
                }
                else {
                // print new messages
                log(1, Strophe.getText(body), convertUTCDateToLocalDate(new Date(sentDate)), true);
                var elem = document.getElementById('log');
                elem.scrollTop = elem.scrollHeight;
                }
     }

    // we must return true to keep the handler alive.  
    // returning false would remove it after it finishes.
    return true;
}

//http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript 
function generateUUID(){
    var d = new Date().getTime();
    if(window.performance && typeof window.performance.now === "function"){
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
            });
    return uuid;
}

function attachToSession(messenger_url, messenger_rid, messenger_sid, messenger_jid, chat_admin, messenger_host) {

    window['ATTACH_SID'] = messenger_sid;
    window['ATTACH_RID'] = messenger_rid;
    window['ATTACH_JID'] = messenger_jid;
    window['CHAT_ADMIN'] = chat_admin;
    window['MESSENGER_HOST'] = messenger_host;
    var BOSH_SERVICE = messenger_url;
    // create the connection and attach it
    connection = new Strophe.Connection(BOSH_SERVICE, {'keepalive':true});
    connection.rawInput = function (data) { 
        //log('RECV: ' + data); 
    };
    connection.rawOutput = function (data) { 
        //log('SENT: ' + data); 
    };
    /*connection.xmlInput = function (body) {
        if (body.childNodes.length > 0) {
            $.each(body.childNodes, function () {
                    $('#log').append("<div>" + Strophe.getText(this) + "</div>");
                    });
        }
    };*/
    window.ATTACH_RID = parseInt(window.ATTACH_RID,10)+1;
        connection.attach(window.ATTACH_JID+"@"+"{{request.session.messenger_host}}", window.ATTACH_SID, window.ATTACH_RID, onConnect);
    // set up handler
    connection.addHandler(onResult, null, 'iq', 
            'result', 'disco-1', null);
    window.connection = connection;
}


function createSession(messenger_url, messenger_jid, chat_admin, messenger_host, messenger_password, callback) {

    window['ATTACH_JID'] = messenger_jid;
    window['CHAT_ADMIN'] = chat_admin;
    window['MESSENGER_HOST'] = messenger_host;
    var BOSH_SERVICE = messenger_url;
    // create the connection and attach it
    connection = new Strophe.Connection(BOSH_SERVICE, {'keepalive':true});
    connection.rawInput = function (data) { 
        //log('RECV: ' + data); 
    };
    connection.rawOutput = function (data) { 
        //log('SENT: ' + data); 
    };
    connection.connect(messenger_jid + '@' + messenger_host, messenger_password, onConnect, false, false, false, false, callback);
    window['ATTACH_SID'] = connection._proto.sid;
    window['ATTACH_RID'] = connection._proto.rid;
    // set up handler
    connection.addHandler(onResult, null, 'iq', 
            'result', 'disco-1', null);
    window.connection = connection;
}


function connectToMessenger(offlineCount, callback) {
    var jqxhr = $.ajax({ url: "../messenger/",
                         dataType: "json"
    }).success(function(chatHistory) {
            //success callback
            if (typeof(callback) != 'function') {
                callback = showStropheError;
            }
            createSession(chatHistory.messenger_url, chatHistory.cn, chatHistory.chat_admin, chatHistory.messenger_host, chatHistory.cp, callback);
            //show Chat History in messenger window
            showChatHistory(chatHistory.history, chatHistory.messenger_jid);
            fadeInNewMessages(offlineCount);
            log(0, gettext("****Connection established."), new Date(Date.now()), false);
            
            //send pres:
            function $pres(attrs) { return new Strophe.Builder("presence", attrs); }
            setTimeout(function(){ connection.send($pres({xmlns: Strophe.NS.CLIENT})); }, 1000);
            
            //scroll down
            var elem = document.getElementById('log');
            elem.scrollTop = elem.scrollHeight;

            })
    .fail(function(exception) {
            log(0, gettext("****Connection failed."), new Date(Date.now()), false);
            console.log( exception );
            });


}

function showStropheError(){
   log(0, gettext("****Connection problem, your message was not sent. Please try again later."), new Date(Date.now()), false);

}

function sendMessage(messageText, wind){
    $('#messengerTextbox').val('');
    if ( messageText != '') {
        if(!window.sessionStorage.getItem('strophe-bosh-session')){
                log(0, gettext("****Attempting to reconnect to chat server..."), new Date(Date.now()), false);
                connectToMessenger(0, showStropheError);
        }
        if (window.sessionStorage.getItem('strophe-bosh-session')) {
                // print the users message on the screen
                log(0, messageText, new Date(Date.now()), true);
                var newRID = parseInt(window.ATTACH_RID,10)+1;
                window.ATTACH_RID = newRID;
                    var newUUID = generateUUID();
                var send_message_xml = "<body "
                    + "rid='" + newRID + "' "
                    + "xmlns='https://jabber.org/protocol/httpbind' "
                    + "sid='" + window.ATTACH_SID + "'>"
                    + "<message "
                    + "id='" + newUUID + "' "
                    + "to='" + window.CHAT_ADMIN + "@" + window.MESSENGER_HOST  + "' "
                    + "type='chat' xmlns='jabber:client'>"
                    + "<body>" + messageText + "</body>"
                    + "</message></body>";
                messageXml = text_to_xml(send_message_xml);
                if(messageXml) {
                    try {
                        connection.send(Strophe.copyElement(messageXml.childNodes[0]), showStropheError);
                        globalMsgIDsAdd(newUUID);
                        //scroll to bottom
                        var elem = document.getElementById('log');
                        elem.scrollTop = elem.scrollHeight;

                        // do the alert for after hours
                        if(AFTER_HOURS_ON){
                            $("#dialog").html(AFTER_HOURS_MESSAGE);
                            $("#dialog").dialog({ 
                            modal:true,
                            buttons: [
                            {
                              text: "OK",
                              click: function() {
                                $("#dialog").html('');
                                $( this ).dialog( "close" );
                              }
                            }
                          ]});
                          $(".ui-dialog-title").html(gettext("STOPit Messenger"));
                        }
                    } catch (e){
                        log(0, gettext("****Your message was not sent, please contact an administrator"), Date.now(), false);
                        console.log(e.message);
                    }
                }
        }
    }
}


function showChatHistory(chatHistoryList, username){
    //chatID, from, chatBody, admin
    for (var key in chatHistoryList) {
        duplicate = globalMsgIDsAdd(chatHistoryList[key].id);
        displayName = 1;
        var from = chatHistoryList[key].fromJID;
        var sentDate = chatHistoryList[key].sentDate;
        var findAt = from.indexOf("@");
        if(from.substr(0,findAt) === username){
            displayName = 0;
        }
        if (!duplicate){
            log(displayName, chatHistoryList[key].body.replace("&apos;","'"), convertUTCDateToLocalDate(new Date(parseInt(sentDate, 10))), false);
        }
    }

}


function convertUTCDateToLocalDate(date) {
    /*var newDate = new Date(date.getTime() + date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);*/
    return new Date(date.getTime());   
}

function getOfflineChatsAjax() {
$.ajax( "/getOfflineMessages/" )
  .done(function(offlinePacket) {
    showOfflineChatCount(parseInt(offlinePacket.offMessagesCount, 10), null);
  })
  .fail(function(xhr, textStatus, errorThrown) {
    //popup
    console.log(xhr.responseText);
    console.log(errorThrown);
    console.log(textStatus);
    $(".ui-dialog").attr("ui-dialog-title", gettext("STOPit Messenger"));
    $("#dialog").html(gettext("Chat error, please notify an administrator."));
    $("#dialog").dialog({modal: true,
    buttons: [
    {
      text: "OK",
      click: function() {
          $("#dialog").html('');
        $( this ).dialog( "close" );
      }
    }
      ]});
            $(".ui-dialog-title").html(gettext("STOPit Messenger"));
  });
}

var AFTER_HOURS_ON = false;
var AFTER_HOURS_MESSAGE = '';
function openChatWindow(afterHoursOn, afterHoursMessage) {
           if(afterHoursOn) {
               AFTER_HOURS_ON = true;
               AFTER_HOURS_MESSAGE = afterHoursMessage;
           }
           clearInterval(getOfflineMessagesInt);
           getOfflineMessagesInt = undefined;
            connectToMessenger(parseInt($('#new_message_count').html(),10));
            $('#chatButton').hide();
            $('#chatButton2').hide();
            $('#blue_dots').hide();
            $('#messengerWindow').show();
            $('#chatBox').show();
            $('#new_message_count').html(''); 
            unBindChatClick();
            // allows messages to be checked when window is not in focus
            if( checkMessagesInt === undefined){
                checkMessagesInt = setInterval(checkForMessages, 1000);
            }
            
}

function closeChatWindow() {
    //hide the messenger window and show the buttons
    $('#messengerWindow').hide();
    $('#chatBox').hide();
    $('#chatButton').show();
    $('#chatButton2').show();
    $('#blue_dots').show();
    $('#chatButton').attr("disabled", false);
    $('#chatButton2').attr("disabled", false);

    //disconnect from messenger
    onDisconnect(Strophe.Status.CONNECTED);
    //restart offline checking
    getOfflineMessagesInt = setInterval(getOfflineChatsAjax, 3000);
    if( checkMessagesInt === undefined){
                checkMessagesInt = setInterval(checkForMessages, 1000);
            }
    return false;
}

function fadeInNewMessages(offlineCount) {
    if(!offlineCount) { // normal check Messages logic
      // print stashed messages
      for (var i =0; i < window.stashedMsgNum; i++) {
          var directionClass = "Right"; 
          if(window.stashedMsgs[i].from) {
              directionClass = "Left";
          }

          $('#log').append('<div class="bubble' + directionClass + '" id="latestMsg" style="display: none;">' + '<span class="bubbleText">' + window.stashedMsgs[i].msg + '</span>' +'</div>');
          $('#log').append('<div class="chatDate' + directionClass + '">' + window.stashedMsgs[i].dateStr + '</div>');
      }

      // find latest msgs and animate it then remove the latest id
      $('div#latestMsg').fadeIn(1000);
      $('div#latestMsg').removeAttr('id');
      var elem = document.getElementById('log');
      elem.scrollTop = elem.scrollHeight;

      //reset stashed Msg array
      window.stashedMsgNum = 0;
      window.stashedMsgs = [];
    }
    else { 
        //offline/stashedMessages fade In logic
        //find last "offlinecount" messages*2 (to catch date strings as well) and attach latestMsg tag to them
      var latestMsgArray = $('div.bubbleLeft, div.bubbleRight, div.chatDateLeft, div.chatDateRight').slice(-offlineCount*2);
      latestMsgArray.attr("id", "latestMsg");
      latestMsgArray.css("display", "none");
      // find latest msgs and animate it then remove the latest id
      latestMsgArray.fadeIn(1000);
      latestMsgArray.removeAttr('id');
      var elem = document.getElementById('log');
      elem.scrollTop = elem.scrollHeight;

    }
}

function unBindChatClick() {
    $("#chatButton").attr("disabled", true);
    $("#chatButton2").attr("disabled", true);
}

function checkForMessages() {
    if(!document.hasFocus() && window.stashedMsgNum){
    // kill check messages for now to stop multiple creation of title flashes
    clearInterval(checkMessagesInt);
    checkMessagesInt = undefined;
    //flash on title
           var counter = 0;
           while (counter < 10) {
                  window.setTimeout(function() { document.title = "STOPit"; } , 300+(counter*600));
                  window.setTimeout(function() { document.title = gettext("New Message"); } , 600+(counter*600));
                  counter++;
          }
    //popup
    if($("#dialog").html() == ''){
    $(".ui-dialog").attr("ui-dialog-title", gettext("STOPit Messenger"));
    $("#dialog").html(gettext("You have one or more new messages pending."));
    $("#dialog").dialog({ 
        modal: true,
        buttons: [
                {
                  text: "OK",
                  click: function() {
                    document.title = "STOPit";
                    fadeInNewMessages(null);
                    //restart check for messages
                    if(checkMessagesInt === undefined) {
                       checkMessagesInt = setInterval(checkForMessages, 1000);
                    }
                    $("#dialog").html('');
                    $( this ).dialog( "close" );
                  }
 
                }
                                             ]});
            $(".ui-dialog-title").html(gettext("STOPit Messenger"));
    }
   }
    else if(document.hasFocus() && window.stashedMsgNum){ //happens when chat window is closed
    // kill check messages for now to stop multiple creation of title flashes
    clearInterval(checkMessagesInt);
    checkMessagesInt = undefined;
    //do the badge flash
    $('#new_message_count').html(window.stashedMsgNum);
    //popup
    if($("#dialog").html() == ''){
    $(".ui-dialog").attr("ui-dialog-title", gettext("STOPit Messenger"));
    $("#dialog").html(gettext("You have one or more new messages pending."));
    $("#dialog").dialog({ 
        modal: true,
        buttons: [
                {
                  text: "OK",
                  click: function() {
                    document.title = "STOPit";
                    fadeInNewMessages(null);
                    //restart check for messages
                    if(checkMessagesInt === undefined) {
                       checkMessagesInt = setInterval(checkForMessages, 1000);
                    }
                    $("#dialog").html('');
                    $("#chatButton").click();  
                    $( this ).dialog( "close" );
                  }
 
                }
                                             ]});
            $(".ui-dialog-title").html(gettext("STOPit Messenger"));
    }
   }
}

function showOfflineChatCount(count, offlineMessages) {
    if(count > 0 ){
        //do the badge flash
        $('#new_message_count').html(count);
        // do the alert
    $("#dialog").html(gettext("You have one or more new messages pending."));
    $("#dialog").dialog({ 
    modal:true,
    buttons: [
    {
      text: "OK",
      click: function() {
        $("#chatButton").click();  
        $("#dialog").html('');
        $( this ).dialog( "close" );
      }
 
    }
  ]});
            $(".ui-dialog-title").html(gettext("STOPit Messenger"));

    }
}


function showLogoutConfirmation(){
    //popup
    if($("#dialog").html() == ''){
    $(".ui-dialog").attr("ui-dialog-title", gettext("STOPit Messenger"));
    $("#dialog").html(gettext("Are you sure you want to enter a new access code?\nOnly select OK if you need to change the\norganization that receives your reports."));
    $("#dialog").dialog({ 
        modal: true,
        buttons: [
                {
                  text: "OK",
                  click: function() {

                    $("#dialog").html('');
                    $( this ).dialog( "close" );
                    location.href = '/logout/';
                  }
 
                }
                                             ]});
            $(".ui-dialog-title").html(gettext("STOPit Messenger"));
    }
}
