var ACCESS_CODE_MAX_LENGTH = 15
var ACCESS_CODE_MIN_LENGTH = 3

function widthofpr(p){
 if(p==0){return "0%";}
 return (p/ACCESS_CODE_MAX_LENGTH)*100;
}

function textofpr(p){
     if(p==0)
     {
        return gettext("Please enter your 3-15 digit access code below.");
     }
     else if(p < ACCESS_CODE_MIN_LENGTH)
     {
         var interpStr = ngettext("Need at least %s more character", "Need at least %s more characters", ACCESS_CODE_MIN_LENGTH - p);
         var s = interpolate(interpStr, [ACCESS_CODE_MIN_LENGTH - p]);
        return s;
     }
     else if(p >= ACCESS_CODE_MIN_LENGTH && p <= ACCESS_CODE_MAX_LENGTH)
     {
        return "";
     }
     else
     {
         return gettext("Too many characters");
     }
}


$(document).ready(function(){
    v=$("#pass");
    p=$("#pbar");
    pt=$("#ppbartxt");
    pet=$("#ppbarerrortxt");

    v.bind('keydown', function(e){
    if (e.keyCode == 32)
        return false;
    });

    v.bind('keyup',function(){
        var access_code_len = $.trim(v.val()).length;
        p.css('width',widthofpr(access_code_len) );
        pt.text(textofpr(access_code_len));
        if (access_code_len >= ACCESS_CODE_MIN_LENGTH && access_code_len <= ACCESS_CODE_MAX_LENGTH)
        {
            $("#next_btn").prop( "disabled", false );
        }else{
             $("#next_btn").prop( "disabled", true );
        }
        if (access_code_len > 0)
        {
            pet.text("");
        }
    });

});

