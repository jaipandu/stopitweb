/*jslint browser: true*/
/*global $, jQuery, alert, confirm, gettext*/
$(document).ready(function () {
    var v = $("#notes"),
        org_contact = $("#orgcontact"),
        attached_files = $("#attachedfiles")


    $(".anonymous").change(function() {
        if (!this.checked) {
            $("#dialog").html("This report will no longer be submitted anonymously.");
            $("#dialog").dialog({ buttons: [
    {
      text: "OK",
      click: function() {
        $("#dialog").html('');
        $( this ).dialog( "close" );
      }
    },
    {
      text: "Cancel",
      click: function() {
          $("#dialog").html('');
          $(".anonymous").prop('checked', true);
          $( this ).dialog( "close" );
      }
    }
      ]});
            $(".ui-dialog-title").html("Are you sure?");
    }
    });

    org_contact.bind('click', function () {
        if ($("#org_contact_mandatory").val() === "1") {
            $("#dialog").html("Sorry – your organization has set up this contact to receive all incident reports.");
            $("#dialog").dialog({ buttons: [
    {
      text: "OK",
      click: function() {
          $("#dialog").html('');
        $( this ).dialog( "close" );
      }
    }
      ]});
            $(".ui-dialog-title").html("Please Note");
            return false;
        }
        return true;
    });

    $('#incidentform').submit(function () {
        if ($('[name="report_contact"]:checked').length === 0) {
            $("#dialog").html("Please select who should receive this report");
            $("#dialog").dialog({ 
                modal:true,
                buttons: [
    {
      text: "OK",
      click: function() {
          $("#dialog").html('');
        $( this ).dialog( "close" );
      }
    }
      ]});
            $(".ui-dialog-title").html("Please Note");
            return false;
        }
        if (($.trim(v.val()).length === 0) && (attached_files.val().length === 0)) {
            $(".ui-dialog").attr("ui-dialog-title", "Please Note");
            $("#dialog").html(gettext("To send a report, please enter text or attach at least one file."));
            $("#dialog").dialog({ buttons: [
    {
      text: "OK",
      click: function() {
          $("#dialog").html('');
        $( this ).dialog( "close" );
      }
    }
      ]});
            return false;
        }

    return true;
});

});

